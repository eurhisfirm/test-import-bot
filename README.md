# Test Import bot

Do mass insert with a bot to test optimization strategies

## Settings

copy `.env.example` file into `.env` and adapt content to your needs

## Install

Preferably use a `virtualenv`:

```bash
python -m venv .venv
source .venv/bin/activate
```

Install dependencies:

```
pip install -r requirements.txt
```

### Dev tools

Install dev dependencies:

```bash
pip install -r requirements-dev.txt
```

## run

```bash
python test-import.py
```

Tip: use `--help` parameter to get usage info
