import typer
from dotenv import load_dotenv
from wikidataintegrator import wdi_core, wdi_login

load_dotenv()  # take environment variables from .env.


def main(
    api_url: str = typer.Option(..., envvar="MEDIAWIKI_API_URL"),
    user: str = typer.Option(..., envvar="MEDIAWIKI_BOT_USERNAME"),
    password: str = typer.Option(..., envvar="MEDIAWIKI_BOT_PASSWORD"),
    sparql_endpoint_url: str = typer.Option(..., envvar="SPARQL_ENDPOINT_URL"),
):

    login_instance = wdi_login.WDLogin(
        mediawiki_api_url=api_url,
        user=user,
        pwd=password,
    )

    LocalItemEngine = wdi_core.WDItemEngine.wikibase_item_engine_factory(
        mediawiki_api_url=api_url,
        sparql_endpoint_url=sparql_endpoint_url,
    )

    wd_item = LocalItemEngine(new_item=True)
    wd_item.set_label("salut")
    wd_item.write(login_instance)


if __name__ == "__main__":
    typer.run(main)
